Class Assignment 6
===================

Pipelines with Jenkins Demonstration
-------------

## Step 1
Create CA6 folder

I downloaded the teacher link:

     https://github.com/atb/vagrant-with-ansible

I went to the command line directly to the CA6-Part1 folder and ran the following commands:

     vagrant up (folder we have the project) - Create the 3 virtual boxes!
     vagrant ssh ansible - Enter the virtual machine that have ansible
	 cd /vagrant - Change to the folder that have vagrant
     ansible-playbook playbook1.yml -Execute ansible playbook


## Step 2
Add errai-demonstration - Errai demonstration.war from a previous CA

-----EDIT PLAYBOOK------

     - Copy errai to host 1:

     copy: remote_src=no src=/vagrant/errai-demonstration-gradle.war 
     dest=/opt/wildfly/standalone/deployments/		
		
     - Create a task on host2 to download and install H2 Database

     - hosts: host2
     become: yes 
     tasks:  
     - name: install H2     
     get_url: url=http://repo2.maven.org/maven2/com/h2database/h2/1.4.199/h2-1.4.199.jar dest=/home/vagrant
     - name: run H2 
     shell: java -cp /home/vagrant/h2-1.4.199.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &

## Step 3
Change Errai to save in database instead memory

 -----EDIT Playbook-----

    - name: Change h2 database to host2
      replace:
        path: /opt/wildfly/standalone/configuration/standalone.xml
        regexp: '<connection-url>jdbc:h2:mem;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>'
        replace: '<connection-url>jdbc:h2:tcp://192.168.33.12:9092//vagrant/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>'
     - name: Change password h2 database to host2
      replace:
        path: /opt/wildfly/standalone/configuration/standalone.xml
        regexp: '<password>sa</password>'
        replace: '<password></password>'

----Edit Vagrant file-----

     run always H2 (before) host 1 run Errai

     host2.vm.provision "shell", run: "always", inline: <<-SHELL
	   java -cp ./h2-1.4.199.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists &
     SHELL

## Step 4
Install jenkings on host with ansible

Using the vagrant file we install Jenkings on Host with ansible, when we start vagrant

     SHELL
     wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
     sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
     sudo apt-get update && sudo apt-get upgrade
     sudo apt-get install -y jenkins
     SHELL

##Step 5 
Add a new stage on jenkinsfile to run ansible

----Edit Jenkins File-----
	
     pipeline {
     agent any

     stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'Credentials', url: 'https://1070189@bitbucket.org/1070189/devops-18-19-atb-1070189_v3.git'
            }
        }
        
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                bat 'cd PART2 && gradlew clean assemble'
            }
        }
        
        stage('Test') {
            steps {
                echo 'Testing...'
                bat 'cd PART2 && gradlew test'
                junit '**\\test-results\\test\\*.xml'
            }
        }
        
         stage('Javadoc') {
            steps {
                echo 'Javadoc...'
                bat 'cd PART2 && gradlew javadoc'
            }
        }
        
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'PART2/build/libs/*'
            }
		}
        
		stage('Publish image') {
            steps {
				echo 'Publishing image...'
                 script {
                    dockerImage = docker.build ("1070189/ca5-part2:$BUILD_NUMBER","part2/errai-demonstration-gradle-master").push()
             }
		  }
	    }
		
		stage('Ansible Init') {
            steps {
                script {    
               def tfHome = tool name: 'Ansible'
                env.PATH = "${tfHome}:${env.PATH}"
                 sh 'ansible --version'       
            }
            }
        }
        stage('Ansible Deploy') {   
            steps {    
              dir('dev/ansible'){ 
               sh 'ansible all -m ping -i hosts'    
              }
              }
          }
        }
        }
  
 2. Create a new project on jenkings! Indicate the repository, credentials and location of Jenkings file!
 
 3. Build

 ##Step 6 

 Finally add the tag for this new version: CA6
 
         $ git tag -a CA6 -m "CA6"
         $ git push origin CA6
          
And we do the last commit

        $ git commit -a -m "Finish CA6"
        $ git push origin master	
